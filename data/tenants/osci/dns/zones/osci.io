$TTL 1D
@				IN SOA		ns1.osci.io. hostmaster.osci.io. (
						2019041100	; serial (YYYYMMDDRR)
						3600		; Refresh
						1800		; Retry
						604800		; Timeout
						86400 )		; Negative answer TTL
				IN NS		ns1
				IN NS		ns2
				IN NS		ns3
				IN MX		10	mx1
				IN MX		20	mx2
				IN MX		20	mx3
                                IN A            8.43.85.237
                                IN AAAA         2620:52:3:1:5054:ff:fe64:ab5a
; DigitCert
                                IN TXT		"619488  domainadmin@redhat.com"

                IN      CAA     0 issue "digicert.com"
                IN      CAA     0 issue "letsencrypt.org"
                IN      CAA     0 issuewild "\;"
                IN      CAA     0 iodef "mailto:root@osci.io"

; physical machines
speedy				IN A		8.43.85.225
                                IN AAAA         2620:52:3:1:e61f:13ff:fe6b:7334
guido				IN A		8.43.85.226
                                IN AAAA         2620:52:3:1:e61f:13ff:fe6a:d540
seymour                         IN A            8.43.85.208
jerry                           IN A            8.43.85.207
                                IN AAAA         2620:52:3:1:ec4:7aff:fef7:a6f8

; basic domain services
polly				IN A		8.43.85.229
                                IN AAAA         2620:52:3:1:5054:ff:fe61:dcdd
francine			IN A		8.43.85.230
                                IN AAAA         2620:52:3:1:5054:ff:fe8c:ad7a
carla                           IN A            209.132.185.11
; asked for IPv6 for Carla, WIP
ns1				IN A		8.43.85.229
                                IN AAAA         2620:52:3:1:5054:ff:fe61:dcdd
ns2				IN A		8.43.85.230
                                IN AAAA         2620:52:3:1:5054:ff:fe8c:ad7a
ns3                             IN A            209.132.185.11
; keep me A/AAAA RR please, CNAME would break mails
mx1				IN A		8.43.85.229
                                IN AAAA         2620:52:3:1:5054:ff:fe61:dcdd
mx2				IN A 		8.43.85.230
                                IN AAAA         2620:52:3:1:5054:ff:fe8c:ad7a
mx3                             IN A            209.132.185.11
; asked for IPv6 for Carla, WIP
mail				IN CNAME	polly
ntp1				IN CNAME	speedy
ntp2				IN CNAME	guido
catton                          IN A            8.43.85.192
                                IN AAAA         2620:52:3:1:ae1f:6bff:fe1c:17f6

soeru                           IN A            8.43.85.211
                                IN AAAA         2620:52:3:1:5054:ff:fed7:4b82

; websites
tickets				IN A		8.43.85.231
                                IN AAAA         2620:52:3:1:5054:ff:fe92:68db
www                             IN A            8.43.85.237
                                IN AAAA         2620:52:3:1:5054:ff:fe64:ab5a
pxe                             IN CNAME        www
sup                             IN CNAME        catton
openjdk-sources                 IN A            8.43.85.238
pulp-repo                       IN A            8.43.85.195
pulp-www                        IN A            8.43.85.236
scl-web                         IN A            8.43.85.196
spice-web                       IN A            8.43.85.198
tracker                         IN CNAME        piwik-vm.osci.io.
piwik-vm                        IN A            8.43.85.206
; jd's builder
gnocchi                         IN A            8.43.85.232

; Fedora Atomic
fedora-atomic-1                 IN A            8.43.85.200
fedora-atomic-2                 IN A            8.43.85.201
fedora-atomic-3                 IN A            8.43.85.202
fedora-atomic-4                 IN A            8.43.85.203
fedora-atomic-5                 IN A            8.43.85.204
fedora-atomic-6                 IN A            8.43.85.205

pcp                             IN A            8.43.85.210

gdb-buildbot                    IN A            8.43.85.197

patternfly-forum                IN A            8.43.85.214

openjdk-aarch32                 IN A            51.15.204.18
aojdk                           IN A            8.43.85.32
python-builder-rawhide          IN A            8.43.85.216
kiali-bot                       IN A            8.43.85.217
; Management VLAN
$ORIGIN adm.osci.io.

speedy                          IN A            172.24.31.1
cmm-catatonic                   IN A            172.24.31.4
catton                          IN A            172.24.31.9
sup                             IN CNAME        catton
switch-a1-catatonic             IN A            172.24.31.247
switch-a2-catatonic             IN A            172.24.31.248
conserve                        IN A            172.24.31.249


; Internal VLAN
$ORIGIN int.osci.io.

speedy                          IN A            172.24.32.1
guido                           IN A            172.24.32.2
seymour                         IN A            172.24.32.5
jerry                           IN A            172.24.32.6
raagu                           IN A            172.24.32.7
catton                          IN A            172.24.32.9
sup                             IN CNAME        catton
tempusfugit			IN A		172.24.32.10
osci-web-builder                IN A            172.24.32.11
pulp-web-builder                IN A            172.24.32.12
osas-community-web-builder	IN A		172.24.32.14
ovirt-web-builder		IN A		172.24.32.15
rdo-web-builder                 IN A            172.24.32.16
crow-1                          IN A            172.24.32.18
crow-2                          IN A            172.24.32.19
crow-3                          IN A            172.24.32.20
ovirt-engine                    IN A            172.24.32.21
test                            IN A            172.24.32.32

; Services VLAN
$ORIGIN srv.osci.io.

speedy                          IN A            172.24.33.1
guido                           IN A            172.24.33.2
seymour                         IN A            172.24.33.5
jerry                           IN A            172.24.33.6
lucille                         IN A            172.24.33.8
catton                          IN A            172.24.33.9
sup                             IN CNAME        catton
gdb-buildbot                    IN A            172.24.33.10
crow-1                          IN A            172.24.33.11
crow-2                          IN A            172.24.33.12
crow-3                          IN A            172.24.33.13

; OKD cluster
$ORIGIN okd.osci.io.

master1                         IN A            172.24.32.22
infra1                          IN A            172.24.32.23
node1                           IN A            172.24.32.24
node2                           IN A            172.24.32.25
node3                           IN A            172.24.32.26
master2                         IN A            172.24.32.27
master3                         IN A            172.24.32.28
infra2                          IN A            172.24.32.29
infra3                          IN A            172.24.32.30
lb                              IN A            172.24.32.31
*.apps                          IN CNAME        infra1
