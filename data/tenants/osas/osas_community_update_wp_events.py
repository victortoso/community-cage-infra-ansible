#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Script to update the event calendar on WP using the original
# Middleman site
# Copyright (c) 2018 Marc Dequènes (Duck) <duck@redhat.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys
import os
import subprocess
import atexit
import argparse
import shutil


parser = argparse.ArgumentParser(description="Update calendar events on WordPress based "
                                             "on changes in the build")
parser.add_argument("-d", "--debug", help="show debug output",
                    action="store_true")
args = parser.parse_args()

def log_print(message):
    try:
        log_fd
    except NameError:
        pass
    else:
        log_fd.write(message + "\n")
        log_fd.flush()


def debug_print(message):
    if args.debug:
        print(message)
    log_print(message)

# TODO complete that
def notify_error(stage, error):
    print(error)
    sys.exit(3)


name = 'osascommunity-wp-events'

lock_file = os.path.expanduser('{}/lock_{}'.format(
    os.environ.get('XDG_RUNTIME_DIR', '~'), name))
if os.path.exists(lock_file):
    # TODO verify if the PID in the file still exist
    debug_print("osascommunity-wp-events script already running, exiting")
    sys.exit(2)

# TODO try/except, show a better error message
fd = os.open(lock_file, os.O_CREAT | os.O_EXCL | os.O_WRONLY)
os.write(fd, str(os.getpid()))
os.close(fd)
atexit.register(os.unlink, lock_file)

log_file = os.path.expanduser('~/%s.log' % name)
log_fd = open(log_file, "w")

checkout_dir = '/srv/builder/osas_community_wp'

os.chdir(checkout_dir)
# force update, crushing local changes
try:
    result = subprocess.check_output(['git', 'fetch', '-q'], stderr=subprocess.STDOUT)
    debug_print(result)
    result = subprocess.check_output(['git', 'stash'], stderr=subprocess.STDOUT)
    debug_print(result)
    result = subprocess.check_output(['git', 'stash', 'clear'], stderr=subprocess.STDOUT)
    debug_print(result)
    result = subprocess.check_output(['git', 'pull', '--rebase'], stderr=subprocess.STDOUT)
    debug_print(result)
except subprocess.CalledProcessError as C:
    notify_error('setup', C.output)


site_dir = '/srv/builder/osas_community_blog/build'

try:
    shutil.copy2('%s/events/all-current/index.html' % site_dir, '%s/data/calendar/all-current.html' % checkout_dir)
    shutil.copy2('%s/events/calendar.json' % site_dir, '%s/data/calendar/' % checkout_dir)
    shutil.copy2('%s/events/all.ics' % site_dir, '%s/data/calendar/' % checkout_dir)
except IOError as C:
    notify_error('install', C.output)

result = subprocess.call(['git', 'diff', '--quiet', '--exit-code'])
if result == 0:
    debug_print("no changes in the events")
    sys.exit(0)
elif result != 1:
    notify_error('pre-deploy', "checking for changes in the workdir failed")

try:
    result = subprocess.check_output(['git', 'commit', '-a', '-m', 'update events page'], stderr=subprocess.STDOUT)
    debug_print(result)
    result = subprocess.check_output(['git', 'push'], stderr=subprocess.STDOUT)
    debug_print(result)
except subprocess.CalledProcessError as C:
    notify_error('deploy', C.output)

